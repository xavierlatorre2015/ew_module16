class ew_module16 {
    include '::ew_module16::pm_apache'
    include '::ew_module16::pm_php'
    include '::ew_module16::pm_mysql'
    include '::ew_module16::pm_time'
    include '::ew_module16::pm_files'
    include '::ew_module16::pm_firewall'
    include '::ew_module16::pm_redis'
}
