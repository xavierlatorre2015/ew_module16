class ew_module16::pm_mysql {
	include ::yum::repo::mysql_community

	class { 'mysql':
		require => Yumrepo["mysql56-community"],
	}

	mysql::grant { 'mympwar':
		mysql_user     => 'mpwar',
		mysql_password => 'mpwardb',
		require        => Package['mysql']
	}

	mysql::grant { 'mpwar_test':
		mysql_user     => 'mpwar',
		mysql_password => 'mpwardb',
		require        => Package['mysql']
	}
}